﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaw : MonoBehaviour
{
    [SerializeField]
    public float maxRate;
    [SerializeField]
    public float minRate;
    [SerializeField]
    public float rateSpaw;
    [SerializeField]
    public float maxBlock;
    [SerializeField]
    private float currentRateSpaw;
    [SerializeField]
    public GameObject prefab;

    public List<GameObject> blocks;
    void Start()
    {
        for (int i = 0; i < maxBlock; i++)
        {
            GameObject tempblock = Instantiate(prefab) as GameObject;
            tempblock.SetActive(false);
            blocks.Add(tempblock);


        }
    }

    // Update is called once per frame
    void Update()
    {
        currentRateSpaw += Time.deltaTime;
        if (currentRateSpaw > rateSpaw)
        {
            currentRateSpaw = 0;
            spaw();
        }
    }
    private void spaw()
    {
        float posi;
            posi = Random.Range(minRate, maxRate);

        GameObject tempblock = null;
        for (int i = 0; i < maxBlock; i++)
        {
            if (blocks[i].activeSelf == false)
            {
                tempblock = blocks[i];
                break;
            }

        }
        if (tempblock != null)
        {
            tempblock.transform.position = new Vector3(transform.position.x,posi, transform.position.z);
            tempblock.SetActive(true);
        }
    }
}
